﻿using CodeAlgorithms.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestCodeAlgorithms
{
    [TestClass]
    public class MyStackExpressionTest
    {
        [TestMethod]
        public void TestMyStackExpression()
        {
            MyStackExpression mse = new MyStackExpression();
            Assert.IsTrue(mse.ValidateParentheses(""));
            Assert.IsTrue(mse.ValidateParentheses("()"));
            Assert.IsTrue(mse.ValidateParentheses("(3(5)4)"));
            Assert.IsFalse(mse.ValidateParentheses("(()))"));

            string expr = "(3+2) + 3 + (3+4)";
            mse.CalculateExpression(expr);
            
        }
    }
}
