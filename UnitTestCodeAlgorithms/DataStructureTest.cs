using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeAlgorithms.DataStructure;

namespace UnitTestCodeAlgorithms
{
    [TestClass]
    public class DataStructureTest
    {
        [TestMethod]
        public void TestMyStackSize()
        {
            MyStack s = new MyStack(5);
            Assert.AreEqual(5, s.Size);
            
            for (int i = 1; i <= 5; i++)
            {
                s.Push(i);
                Assert.AreEqual(i, s.Top);
            }
            
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void TestMyStackOverflow()
        {
            MyStack myStack = new MyStack();

            for (int i = 10; i < 22; i++)
            {
                myStack.Push(i);
            }

        }

        [TestMethod]
        public void TestMyQueueSize()
        {
            MyQueue q = new MyQueue(5);
            Assert.AreEqual(5, q.Size);
            for (int i = 1; i <= 5; i++) {
                q.Enqueue(i);
            }
            Assert.AreEqual(5, q.Count);

            q.Dequeue();
            q.Enqueue(6);
            Assert.AreEqual(2, q.Dequeue());
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void TestMyQueueOverflow()
        {
            MyQueue q = new MyQueue(4);
            
            for (int i = 1; i <= 5; i++)
            {
                q.Enqueue(i);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestMyQueueUnderflow()
        {
            MyQueue q = new MyQueue(3);

            for (int i = 1; i <= 3; i++)
            {
                q.Enqueue(i);
            }

            for (int i = 1; i <= 4; i++)
            {
                q.Dequeue();
            }
        }

        [TestMethod]
        public void TestMyLinkedList()
        {
            SinglyLinkedList sll = new SinglyLinkedList();
            Assert.IsNull(sll.Head);

            sll.Insert(11);
            Assert.IsNotNull(sll.Head);
            Assert.AreEqual(11, sll.Head.Value);
            Assert.IsNull(sll.Head.Next);

            sll.Insert(22);
            Assert.IsNotNull(sll.Head);
            Assert.AreEqual(22, sll.Head.Value);
            Assert.IsNotNull(sll.Head.Next);
            Assert.AreEqual(11, sll.Head.Next.Value);

            sll.Insert(33);
            sll.Insert(44);
            SinglyLinkedElement e = sll.Search(33);
            Assert.IsNotNull(e);
            Assert.AreEqual(33, e.Value);
            Assert.AreEqual(22, e.Next.Value);

            Assert.IsNull(sll.Search(0));

            Assert.IsTrue(sll.Delete(33));
            Assert.AreEqual(22, sll.Search(44).Next.Value);
        }
    }
}
