﻿using CodeAlgorithms.DataStructure;
using System;

namespace CodeAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Algorithms Study");

            MyStackExpression mse = new MyStackExpression();
            string expr = "(3+2) + (3 + (3+4+5))";
            mse.CalculateExpression(expr);
           

            Console.ReadLine();
        }        
    }
}