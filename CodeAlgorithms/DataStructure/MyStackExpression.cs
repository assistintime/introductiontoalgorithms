﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeAlgorithms.DataStructure
{
    public class MyStackExpression
    {
        MyStack _exprStack;
        int _leftOperand, _rightOperand;
        enum Operators { Add, Deduct};
        Operators _operator;

        public bool ValidateParentheses(string expr)
        {
            _exprStack = new MyStack(expr.Length);
            try
            {
                foreach (Char c in expr.ToCharArray())
                {
                    switch (c)
                    {
                        case '(':
                            _exprStack.Push(1);
                            break;
                        case ')':
                            _exprStack.Pop();
                            break;
                    }
                }
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            return _exprStack.IsEmpty;
        }

        public int CalculateExpression(string expr)
        {
            int result = 0;
            int count = 0;
            string localExpr = "(" + expr + ")";

            if (!ValidateParentheses(localExpr))
            {
                throw new ArgumentException();
            }
            
            _exprStack = new MyStack(localExpr.Length);

            foreach (Char c in localExpr.ToCharArray())
            {
                switch (c)
                {
                    case ' ':
                        // ignore white spaces
                        break;
                    case ')':
                        // pop all chars in the current parentheses
                        Char[] tempString = new Char[count];
                        Char temp = (Char)_exprStack.Pop();
                        string r = "", l = "";
                        
                        bool isRight = true;
                        while (!temp.Equals('('))
                        {
                            switch (temp)
                            {
                                case '+':
                                    _operator = Operators.Add;
                                    isRight = false;
                                    break;
                                case '-':
                                    _operator = Operators.Deduct;
                                    isRight = false;
                                    break;
                                default:
                                    if (isRight)
                                    {
                                        r += temp;
                                    }
                                    else
                                    {
                                        l += temp;
                                    }
                                    break;
                            }
                            tempString[--count] = temp;
                            temp = (Char)_exprStack.Pop();                                                       
                        }
                        Console.WriteLine(String.Format("exp: {0} {1} {2}", l, _operator.ToString(), r));
                        break;
                    default:
                        _exprStack.Push(c);
                        count++;
                        break;
                }
            }


            return result;
        }


    }

    
}
