﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeAlgorithms.DataStructure
{
    public class SinglyLinkedElement
    {        
        public SinglyLinkedElement Next
        {
            get;set;
        }

        public int Value
        {
            get;set;
        }
    }

    class DoublyLinkedElement : SinglyLinkedElement
    {
        public DoublyLinkedElement Previous
        {
            get;set;
        }
    }

    public class SinglyLinkedList
    {
        SinglyLinkedElement _head;

        public SinglyLinkedElement Head
        {
            get { return _head; }
        }

        public void Insert(int i)
        {
            SinglyLinkedElement e = new SinglyLinkedElement();
            e.Value = i;
            if(null == _head)
            {
                _head = e;
            }
            else
            {
                e.Next = _head;
                _head = e;
            }
            
        }

        public SinglyLinkedElement Search(int i)
        {
            if (null != _head)
            {
                if (_head.Value == i)
                {
                    return _head;
                }
                else
                {
                    SinglyLinkedElement current = _head;
                    while (null != current.Next)
                    {
                        current = current.Next;
                        if (current.Value == i)
                        {
                            return current;
                        }
                    }
                }             
            }
            return null;
        }

        public bool Delete(int i)
        {
            if (null != _head)
            {
                if (_head.Value == i)
                {
                    _head = _head.Next;
                    return true;
                }
                else
                {
                    SinglyLinkedElement current = _head;
                    while (null != current.Next)
                    {                        
                        if (current.Next.Value == i)
                        {
                            current.Next = current.Next.Next;
                            return true;
                        }
                        current = current.Next;
                    }
                }
            }
            return false;
        }


    }
}
