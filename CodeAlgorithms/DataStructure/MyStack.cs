﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeAlgorithms.DataStructure
{
    public class MyStack
    {
        private static int _DEFAULT_STACK_SIZE = 10;
        private int _stackSize;
        private object[] _array;
        private int _count; // the value of _count is the index for the next element

        public MyStack()
        {
            _stackSize = 0 == _stackSize ? _DEFAULT_STACK_SIZE : _stackSize;
            Initilize();
        }

        public MyStack(int size) : this()
        {
            if(size < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            _stackSize = size;
            Initilize();
        }

        private void Initilize()
        {
            _array = new object[_stackSize];
            _count = 0;
        }

        public bool IsEmpty
        {
            get { return 0 == _count; }
        }

        public int Size
        {
            get { return _stackSize; }
        }

        /** Returns the most recent inserted element. Exception if the stack is empty.
         */
        public object Top
        {
            get { if (!IsEmpty) { return _array[_count - 1]; } else { throw new InvalidOperationException(); } }
        }

        public void Push(object o)
        {
            if (_count < _stackSize)
            {
                // push the element in, and update the count, which also prepare it for the next element index
                _array[_count] = o;
                _count++;
            }
            else
            {
                throw new OverflowException();
            }      
        }

        public object Pop()
        {
            if (!IsEmpty)
            {
                _count--;
                return _array[_count];
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}
