﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeAlgorithms.DataStructure
{
    public class MyQueue
    {
        int _size;
        int[] _array;
        int _count;
        int _head;//index of the first element
        int _tail;//index of the next empty space

        public MyQueue(int size)
        {
            if (size > 0)
            {
                _size = size;
                _array = new int[_size];
                _count = 0;
                _head = 0;
                _tail = 0;
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public MyQueue() : this(10) { }
      
        public int Size
        {
            get { return _size; }
        }

        public int Count
        {
            get { return _count; }
        }
      
        public void Enqueue(int i)
        {
            if(_count < _size)
            {
                _array[_tail] = i;
                _count++;
                _tail++;
                if(_tail > _size)
                {
                    _tail = _tail - _size - 1;
                }
            }
            else
            {
                throw new OverflowException();
            }
        }

        public int Dequeue()
        {
            if(_count > 0)
            {
                int temp = _head;
                _count--;
                _head++;
                if(_head > _size)
                {
                    _head = _head - _size - 1;
                }
                return _array[temp];
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}
